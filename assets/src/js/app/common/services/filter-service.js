(function(angular, _) {
  'use strict';

  angular
    .module('app.services.filterService', [])
    .factory('filterService', function filterService() {
      /**
       * Does the input have a given taxonomy with a given term?
       */
      var filterByTerm = function(input, taxonomy, tid) {
        if (validateInput(input, taxonomy)) {
          return (input[taxonomy].id == tid) ? input : false;
        }
      };

      /**
       * Does the input have a given taxonomy with a given term?
       */
      var filterByTerms = function(input, taxonomy, tid) {
        var result = false;

        if (validateInput(input, taxonomy)) {
          _.each(input[taxonomy], function(item) {
            if (item.id == tid) {
              result = true;
              return false;
            }
          });
        }

        return result;
      };

      return {
        filterByTerm: filterByTerm,
        filterByTerms: filterByTerms
      };

      /**
       * Is the input an object with a given taxonomy?
       */
      function validateInput(input, taxonomy) {
        return (typeof input === 'object' && input[taxonomy]);
      }
    });
})(window.angular, window._);
