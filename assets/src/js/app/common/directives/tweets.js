(function(angular) {
  'use strict';

  angular
    .module('app.directives.tweets', [])
    .directive('tweets', function(SETTINGS) {
      var tweetsDirectiveController = function tweetsDirectiveController(tweetService, $rootScope, $scope) {
        var vm = this;

        // Initialize the controller instance.
        setInitialState();

        /**
         * Set the initial state of the controller.
         */
        function setInitialState() {
          $rootScope.$watch('meta().settings', function(newValue, oldValue) {
            if (newValue && newValue.social) {
              if (newValue.social.hashtag) {
                vm.hashtag = newValue.social.hashtag;
              }

              if (newValue.social.handle) {
                vm.handle = newValue.social.handle;
              }

              if (newValue.twitter_feed) {
                vm.twitter = newValue.twitter_feed;
              }
            }
          });

          $scope.$watch('vm.dataclasses', function(newValue, oldValue) {
            vm.classes = angular.copy(newValue);
          });

          $scope.$watch('vm.datalayout', function(newValue, oldValue) {
            vm.layout = angular.copy(newValue);
          });

          vm.tweets = [];

          loadTweets();
        }

        /**
         * Load tweets and store them in the view-model.
         */
        function loadTweets() {
          vm.loading = true;
          vm.error   = null;
          vm.current = 0;
          vm.max     = 0;

          tweetService
            .getAll(function(data) {
              if (!data || data.length < 1) {
                vm.error = 'No tweets found';
              }

              vm.tweets = data;
              vm.loading = false;
              vm.max = data.length - 1;
            })
          ;
        }

        vm.nextTweet = function() {
          if (vm.current < vm.max) {
            vm.current = vm.current + 1;
          } else {
            vm.current = 0; // Back to the beginning
          }
        };

        vm.previousTweet = function() {
          if (vm.current > 0) {
            vm.current = vm.current - 1;
          } else {
            vm.current = vm.max; // Back to the end
          }
        };
      };

      /**
       * Return the public interface for the directive.
       */
      return {
        restrict: 'EA',
        controller: tweetsDirectiveController,
        controllerAs: 'vm',
        bindToController: true,
        scope: {
          dataclasses: '@',
          datalayout: '@'
        },
        templateUrl: SETTINGS.TEMPLATE_URL + 'directives/tweets.tpl.html'
      }
    })
  ;
})(window.angular);
