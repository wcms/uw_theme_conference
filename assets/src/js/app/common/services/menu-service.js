(function(angular) {
  'use strict';

  angular
    .module('app.services.menuService', [])
    .factory('menuService', function menuService($http, SETTINGS, conferenceCache) {
      var URLS = {
        SHOW: SETTINGS.LIVE_API_URL + 'conference_main_menu'
      };

      var OPTIONS = {
        cache: conferenceCache
      };


      var obj = {
        items: []
      };

      obj.setMenu = function() {
        $http
          .get(URLS.SHOW, OPTIONS)
          .success(function(response) {
            obj.items = response.data;
          })
        ;
      };

      obj.getMenu = function() {
        return obj;
      };

      return obj;
    })
  ;
})(window.angular);
