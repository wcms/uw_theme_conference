(function() {
  // Variable used to track when the form is submitted.
  // i.e to check for double submissions on using enter key in dialog.
  var formSubmitted = false;
  var tableauDialog = function(editor) {
    return {
      title : 'Tableau Properties',
      minWidth : 625,
      minHeight : 150,
      contents: [{
        id: 'tableau',
        label: 'tableau',
        elements:[{
          type: 'text',
          id: 'tableauInput',
          label: 'Tableau name:',
          setup: function(element) {
                   this.setValue(element.getAttribute('data-url'));
                 }
        },{
          type: 'text',
          id: 'heightInput',
          label: 'Tableau height in pixels (minimum 100):',
          setup: function(element) {
                   this.setValue(element.getAttribute('data-height'));
                 }
        },{
          type: 'select',
          id: 'tabsInput',
          label: 'Display tabs:',
          items: [
                   ['Yes', 'yes'],
                   ['No', 'no']
                 ],
          required: true,
          setup: function(element) {
                   this.setValue(element.getAttribute('data-tabs'));
                 }
        }]
      }],
      onOk: function() {
        //get form information
        tableauInput = this.getValueOf('tableau','tableauInput');
        heightInput = this.getValueOf('tableau','heightInput');
        tabsInput = this.getValueOf('tableau','tabsInput');
        //validate input. Note that there is probably a CKEditor specific way to do this, but this works.
        errors = '';
        if (!tableauInput) {
          errors += "You must enter the name of the Tableau visualization.\r\n";
        }
        if (!heightInput || heightInput.NaN || heightInput < 100 || Math.floor(heightInput) != heightInput) {
          errors += "You must enter a valid whole number for the height.\r\n";
        }
        if (!tabsInput) {
          errors += "You must select whether or not to display tabs.\r\n";
        }
        // If form has been submitted before then set it back to not being seeing before.
        // i.e if this is double submission set it back to not being run before
        if(formSubmitted == true) {
          formSubmitted = false;
          return false;
        }
        // Only display erros if there are errors to display and the form has not been run before.
        else if (errors && formSubmitted == false) {
          alert(errors);
          formSubmitted = true;
          return false;
        }
        else {
          //create the cktableau element
          var cktableauNode = new CKEDITOR.dom.element('cktableau');
          //save contents of dialog as attributes of the element
          cktableauNode.setAttribute('data-url',tableauInput);
          cktableauNode.setAttribute('data-height',heightInput);
          cktableauNode.setAttribute('data-tabs',tabsInput);
          //adjust title based on user input
          CKEDITOR.lang.en.fakeobjects.cktableau = CKEDITOR.socialmedia.cktableau+': '+tableauInput;
          //create the fake image for this element and insert into the document (realElement, className, realElementType, isResizable)
          var newFakeImage = editor.createFakeElement(cktableauNode, 'cktableau', 'cktableau', false);
          //set the fake object to the entered height; if there isn't one, use 100 so it's not invisible
          newFakeImage.$.height = heightInput;
          newFakeImage.addClass('cktableau');
          if (this.fakeImage) {
            newFakeImage.replace(this.fakeImage);
            editor.getSelection().selectElement(newFakeImage);
          } else {
            editor.insertElement(newFakeImage);
          }
          //reset title
          CKEDITOR.lang.en.fakeobjects.cktableau = CKEDITOR.socialmedia.cktableau;
        }
      },
      onShow: function() {
        //Set up to handle existing items
        this.fakeImage = this.cktableauNode = null;
        var fakeImage;

        // Check if element is right clicked or icon was clicked, if not use global varaible doubleclick_element
        if (this.getSelectedElement()) {
          fakeImage = this.getSelectedElement();
        }
        else {
          fakeImage = doubleclick_element;
        }

        if (fakeImage && fakeImage.data('cke-real-element-type') && fakeImage.data('cke-real-element-type') === 'cktableau') {
          this.fakeImage = fakeImage;
          var cktableauNode = editor.restoreRealElement(fakeImage);
          this.cktableauNode = cktableauNode;
          this.setupContent(cktableauNode);
        }
      }
    }
  }

  CKEDITOR.dialog.add('tableau', function(editor) {
    return tableauDialog(editor);
  });    
})();
