(function() {
  // Variable used to track when the form is submitted.
  // i.e to check for double submissions on using enter key in dialog.
  var formSubmitted = false;
  var vimeoDialog = function(editor) {
    return {
      title : 'Vimeo Properties',
      minWidth : 625,
      minHeight : 150,
      contents: [{
        id: 'vimeo',
        label: 'vimeo',
        elements:[{
          type: 'text',
          id: 'vimeoInput',
          label: 'Vimeo URL:',
          required: true,
          setup: function(element) {
                   this.setValue(element.getAttribute('data-url'));
                 }
        }]
      }],
      onOk: function() {
        // Get form information.
        vimeoInput = this.getValueOf('vimeo','vimeoInput');
        // Validate input. Note that there is probably a CKEditor specific way to do this, but this works.
        errors = '';

        if (!CKEDITOR.socialmedia.vimeo_url_regex.test(vimeoInput)) {
          errors += "You must enter a valid vimeo URL.\r\n";
        }
        else {
          errors = '';
        }
        if (!vimeoInput) {
          errors += "You must enter the Vimeo URL.\r\n";
        }
        // If form has been submitted before then set it back to not being seeing before.
        // i.e if this is double submission set it back to not being run before
        if(formSubmitted == true) {
          formSubmitted = false;
          return false;
        }
        // Only display erros if there are errors to display and the form has not been run before.
        else if (errors && formSubmitted == false) {
          alert(errors);
          formSubmitted = true;
          return false;
        }
        else {
          // Create the vimeo element.
          var ckvimeoNode = new CKEDITOR.dom.element('ckvimeo');
          // Save contents of dialog as attributes of the element.
          ckvimeoNode.setAttribute('data-url',vimeoInput);
          // Adjust title based on user input.
          CKEDITOR.lang.en.fakeobjects.ckvimeo = CKEDITOR.socialmedia.ckvimeo+': '+vimeoInput;
          // Create the fake image for this element and insert into the document (realElement, className, realElementType, isResizable)
          var newFakeImage = editor.createFakeElement(ckvimeoNode, 'ckvimeo', 'ckvimeo', false);
          // Set the fake object to the entered height; if there isn't one, use 100 so it's not invisible.
          newFakeImage.addClass('ckvimeo');
          if (this.fakeImage) {
            newFakeImage.replace(this.fakeImage);
            editor.getSelection().selectElement(newFakeImage);
          } else {
            editor.insertElement(newFakeImage);
          }
          // Reset title.
          CKEDITOR.lang.en.fakeobjects.ckvimeo = CKEDITOR.socialmedia.ckvimeo;
        }
      },
      onShow: function() {
        // Set up to handle existing items.
        this.fakeImage = this.ckvimeoNode = null;
        var fakeImage;

        // Check if element is right clicked or icon was clicked, if not use global varaible doubleclick_element
        if (this.getSelectedElement()) {
          fakeImage = this.getSelectedElement();
        }
        else {
          fakeImage = doubleclick_element;
        }

        if (fakeImage && fakeImage.data('cke-real-element-type') && fakeImage.data('cke-real-element-type') === 'ckvimeo') {
          this.fakeImage = fakeImage;
          var ckvimeoNode = editor.restoreRealElement(fakeImage);
          this.ckvimeoNode = ckvimeoNode;
          this.setupContent(ckvimeoNode);
        }
      }
    }
  }

  CKEDITOR.dialog.add('vimeo', function(editor) {
    return vimeoDialog(editor);
  });
})();
