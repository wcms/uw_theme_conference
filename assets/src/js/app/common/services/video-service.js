(function(angular) {
  'use strict';

  angular
    .module('app.services.videoService', [])
    .factory('videoService', function videoService($http, SETTINGS, conferenceCache) {

      // REST endpoints to use with the $http service.
      var URLS = {
        INDEX: SETTINGS.LIVE_API_URL + 'conference_videos'
      };

      // HTTP options to use with the $http service.
      var OPTIONS = {
        cache: conferenceCache
      };

      // REST Actions that the videoService can perform via $http service.
      var ACTIONS = {
        /**
         * Get all videos.
         * @async
         * @param {function} callback - Takes array of video {object} as param
         */
        INDEX: function(callback) {
          $http
            .get(URLS.INDEX, OPTIONS)
            .success(function(response) {
              callback(processVideos(response.data));
            })
          ;
        },

        /**
         * Get featured videos.
         * @async
         * @param {function} callback - Takes array of video {object} as param
         */
        FEATURED: function(limit, callback) {
          var url = URLS.INDEX;

          if (limit) {
            url = url + '?filter[promote][value]=1&range=' + limit;
          }

          $http
            .get(url, OPTIONS)
            .success(function(response) {
              callback(processVideos(response.data));
            })
          ;
        }
      };

      /**
       * Process videos by converting youtube URLs to correct format.
       * @param {array} data - An array of video {object}
       * @return {array} - An array of video {object}
       */
      function processVideos(data) {
        var results = [];

        if (data && data.length) {
          for (var i = 0; i < data.length; i++) {
            var item = data[i];
            if (item.video_url) {
              var youtubeId = getYouTubeId(item.video_url);
              if (youtubeId) {
                item.video_url  = 'https://www.youtube.com/embed/' + youtubeId;
                item.video_url += '?modestbranding=1&autohide=1&showinfo=0&controls=1';
              } else {
                var isVimeo = item.video_url.indexOf('https://player.vimeo.com') > -1;
                if (isVimeo) {
                  item.video_url = item.video_url + '?loop=1';
                }
              }
            }
            results.push(item);
          }
        }

        return results;
      }

      function getYouTubeId(url) {
        //var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
        var regExp  = /^https?:\/\/(?:www\.youtube(?:-nocookie)?\.com\/(watch\?(?:\S+&)?v=|embed\/|.+#.+\/)|youtu\.be\/)([\w\-]{11})(?:[#&?]\S*)?$/;
        var match = url.match(regExp);

        if (match && match[2].length == 11) {
            return match[2]; // This is a YouTube ID!
        } else {
            return false;
        }
      }

      // Public interface for the videoService.
      return {
        getAll:      ACTIONS.INDEX,
        getFeatured: ACTIONS.FEATURED
      }

    })
  ;
})(window.angular);
