(function(angular) {
  'use strict';

  /**
   * The speakers directive encapsulates the logic of displaying speakers.
   * This directive includes a feature for displaying additional speaker details
   * in a modal window.
   */
  angular
    .module('app.directives.speakers', [])
    .directive('speakers', function(SETTINGS) {
      var speakersDirectiveController = function(speakerService, $modal, $scope) {
        var vm = this;

        // Initialize the controller instance.
        setInitialState();

        /**
         * Show additional speaker details in a modal window.
         * @param {object} speaker - A speaker instance.
         */
        vm.showSpeaker = function(speaker) {
          var modalInstance = $modal.open({
            animate: true,
            templateUrl: SETTINGS.TEMPLATE_URL + 'components/speaker.modal.tpl.html',
            controller: function($scope, $modalInstance, speaker) {
              $scope.speaker = angular.copy(speaker);
              $scope.close = function() {
                $modalInstance.dismiss('cancel');
              };
            },
            size: 'lg',
            resolve: {
              speaker: speaker
            }
          });
        };

        /**
         * Set the initial state of this controller.
         */
        function setInitialState() {
          vm.speakers = [];
          vm.loading  = false;
          vm.error    = null;

          loadSpeakers();
        }

        /**
         * Load speakers via speakerService and store in the view-model.
         */
        function loadSpeakers() {
          speakerService
            .getAll(function(data) {
              if (!data || data.length < 1) {
                vm.error = 'No speakers found';
              }

              vm.speakers = data;
              vm.speakers = _.sortByAll(vm.speakers, ['last_name', 'first_name']);
              vm.loading  = false;
            })
          ;
        }
      };

      /**
       * Return the public interface for the directive.
       */
      return {
        restrict: 'EA',
        controller: speakersDirectiveController,
        controllerAs: 'vm',
        bindToController: true,
        templateUrl: SETTINGS.TEMPLATE_URL + 'directives/speakers.tpl.html'
      }
    })
  ;
})(window.angular);
