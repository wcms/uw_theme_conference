(function(angular) {
  'use strict';

  angular
    .module('app.controllers', [
      'app.controllers.siteController',
      'app.controllers.leafletController'
    ])
  ;
})(window.angular);