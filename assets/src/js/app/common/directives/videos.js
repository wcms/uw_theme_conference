(function(angular) {

  'use strict';

  angular
    .module('app.directives.videos', [])
    .directive('videos', function(SETTINGS) {

      /**
       * Controller for directive
       * @param {object} videoService - The videoService.
       */
      var videosDirectiveController = function(videoService) {
        var vm = this;

        setInitialState();

        /**
         * Set the initial state of the view-model.
         */
        function setInitialState() {
          vm.videos  = [];
          vm.loading = true;
          vm.error   = null;

          loadVideos(vm);
        }

        /**
         * Load videos via videoService and store in the view-model.
         * Provide error message on view-model to use if no videos found.
         * @param {object} vm - The view model instance.
         */
        function loadVideos(vm) {
          videoService
            .getAll(function(data) {
              if (!data || data.length < 1) {
                vm.error = 'No videos found';
              }

              vm.videos  = data;
              vm.loading = false;
            })
          ;
        }
      };

      // Return the public interface for the directive.
      return {
        restrict: 'EA',
        controller: videosDirectiveController,
        controllerAs: 'vm',
        bindToController: true,
        templateUrl: SETTINGS.TEMPLATE_URL + 'directives/videos.tpl.html'
      };

    })
  ;

})(window.angular);
