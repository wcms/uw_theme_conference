(function(angular, $) {
  'use strict';

  angular
    .module('app.states.blog.controllers.blogShowController', [])
    .controller('blogShowController', function blogShowController(ckeditorSocialMediaService, blogService, metaService, SETTINGS, $modal, $rootScope, $stateParams, $interval) {
      var vm = this;

      setInitialState();
      vm.current_domain = window.location.protocol + '//' + window.location.host;

      vm.showProfile = function(author) {
        var modalInstance = $modal.open({
          animate: true,
          templateUrl: SETTINGS.TEMPLATE_URL + 'components/speaker.modal.tpl.html',
          controller: function($scope, $modalInstance, speaker) {
            $scope.speaker = author;
            $scope.close = function() {
              $modalInstance.dismiss('cancel');
            };
          },
          size: 'lg',
          resolve: {
            speaker: author
          }
        });
      };

      function setInitialState() {
        vm.post = {};
        blogService.getOne($stateParams.slug, handleGetPost);
      }

      function handleGetPost(data) {
        if (!data) {
          data = {
            error: {
              title: 'Page Not Found',
              message: 'The requested page could not be found.'
            }
          };
        } else {
          metaService.updateMetaTags(data.metatags);
        }

        vm.post = data;

        if (!vm.post.error) {
          $interval(function(){
            $('.uw_video-embed').fitVids();
          }, 1000);
        }
      }
    })
  ;
})(window.angular, window.jQuery);
