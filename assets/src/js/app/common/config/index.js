(function(angular) {
  'use strict';

  angular
    .module('app.config', [
      'app.config.logging',
      'app.config.map',
      'app.config.pushstate',
      'app.config.router'
    ])
  ;
})(window.angular);
